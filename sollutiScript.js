function validaForm() {
  emailAlert = "";
  email = formLog.email.value.substring(0, formLog.email.value.indexOf("@"));
  dominio = formLog.email.value.substring(
    formLog.email.value.indexOf("@") + 1,
    formLog.email.value.length
  );
  if (
    email.length >= 1 &&
    dominio.length >= 3 &&
    email.search("@") == -1 &&
    dominio.search("@") == -1 &&
    email.search(" ") == -1 &&
    dominio.search(" ") == -1 &&
    dominio.search(".") != -1 &&
    dominio.indexOf(".") >= 1 &&
    dominio.lastIndexOf(".") < dominio.length - 1
  ) {
    emailAlert = "";
  } else {
    emailAlert = "Email inválido";
  }

  senhaAlert = "";
  senha = formLog.password.value;
  if (senha.length >= 3 && senha.length <= 6) {
    senhaAlert = "";
  } else {
    senhaAlert = "Senha inválida";
  }

  if (senhaAlert === "" && emailAlert === "") {
    alert("Login efeutado com sucesso!");
  } else {
    alert(`${emailAlert}  ${senhaAlert}`);
  }
}
